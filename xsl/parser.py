from lxml import etree

xml_f = '<a><b>Text</b></a>'
xml_tree = etree.fromstring(xml_f)

xsl_path = "xsl/xsl_docs/sample.xsl"

tree = etree.parse(xsl_path)

transform = etree.XSLT(tree)

result = transform(xml_tree)

print(result)