from lxml import etree


students_xml = etree.parse('xsl/students.xml')
students_xsl = etree.parse("xsl/xsl_docs/students.xsl")

transform = etree.XSLT(students_xsl)

result = transform(students_xml)

print(result)