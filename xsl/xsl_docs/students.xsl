<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <xsl:text>{"vencedor":</xsl:text>
        <xsl:apply-templates mode="schoolElem" select="/class/school" />
        <xsl:text>}</xsl:text>
    </xsl:template>

    <xsl:template match="/class/school" mode="schoolElem">
        <xsl:text>{"schoolName": "</xsl:text>
        <xsl:value-of select="nickname" />
        <xsl:text>",</xsl:text>
        
        <xsl:text>"melhorEstudante": "</xsl:text>
        <xsl:value-of select="/class/beststudent/nickname" />
        <xsl:text>"}</xsl:text>
    </xsl:template>

</xsl:stylesheet>