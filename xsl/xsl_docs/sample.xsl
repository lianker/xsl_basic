<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
    <xsl:variable name="my-var">BLA</xsl:variable>
        <foo>
            <xsl:value-of select="/a/b/text()" />
        </foo>
        
        <xsl:text>{"foo": "</xsl:text>
            <xsl:value-of select="/a/b/text()" />
        <xsl:text>"}</xsl:text>

    </xsl:template>
</xsl:stylesheet>
