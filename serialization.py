from lxml import etree

person_xml = "<person><name>John Doe</name><age>31</age></person>"
person_el = etree.XML(person_xml)

print(etree.tostring(person_el))
print(etree.tostring(person_el, xml_declaration=True))
# ASCII is default encode, but is possible use others like utf-8
print(etree.tostring(person_el, encoding='iso-8859-1'))
print(etree.tostring(person_el, pretty_print=True))

# Serializing using methods
print("\nHTML")
print(etree.tostring(person_el, method='html', pretty_print=True))
print("\nTEXT")
print(etree.tostring(person_el, method='text', pretty_print=True))

# ElementTree
print("\nElementTree")

myxml = etree.XML('''\
<?xml version="1.0"?>
<!DOCTYPE root SYSTEM "test" [ <!ENTITY tasty "parsnips"> ]>
<root>
    <a>&tasty;</a>
</root>
''')

tree = etree.ElementTree(myxml)
print(tree.docinfo.xml_version)
print(tree.docinfo.doctype)
tree.docinfo.public_id = '-//W3C//DTD XHTML 1.0 Transitional//EN'
tree.docinfo.system_url = 'file://local.dtd'
print(tree.docinfo.doctype)



