from lxml import etree

root = etree.Element("person")
# Sub Elements
root.append(etree.Element("name"))
root.append(etree.Element("age"))
etree.SubElement(root, "id")
# Attributes
root.append(etree.Element("birthday", day="12", month="Ago", year="1990"))
# Text
root[0].text = "John Doe"




if __name__ == "__main__":
    # Tag
    print(root.tag)
    # All tree
    print(etree.tostring(root, pretty_print=True))
    # Elements seems like python lists
    print(root[0])
    print("Legth: ", len(root))
    print(root.index(root[1]))
    root.insert(0, etree.Element("child0"))
    print(etree.tostring(root, pretty_print=True))

    # sublists
    start = root[:1]
    end = root[-1:]
    print(start[0].tag)
    print(end[0].tag)

    # Attributtes: Works like a dict
    brth = root[-2]
    print(brth.get("year"))

