from lxml import etree
from io import BytesIO

some_xml = "<root>data</root>"

root = etree.fromstring(some_xml)

print(etree.tostring(root))

# XML / HTML
xml = etree.XML("<data>Lorem Ipsum</data>")
print(etree.tostring(xml))

html = etree.HTML("<p>Lorem Ipsum</p>")
print(etree.tostring(html))

# Files or Bytes
some_file_or_file_like_object = BytesIO(b"<root>data</root>")
person_xml = etree.parse("./person.xml")
print("From FILE")
print(etree.tostring(person_xml))

# Returns a ElementTree object instead a Element object
tree = etree.parse(some_file_or_file_like_object)
print(etree.tostring(tree))

